import { Injectable } from '@angular/core';
import { Book } from '../models/Book';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  books:Book[]=[
    new Book(1,"Phool","Story"),
    new Book(2,"Gaban","Novel"),
    new Book(3,"Godan","Novel"),
    new Book(4,"Pratigya","Story"),
    new Book(5,"Mansarovar","Story"),
    new Book(6,"Kaphan","Story"),
    new Book(7,"Sevasadan","Novel"),
    new Book(8,"Karbala","Horror"),
    new Book(9,"A Winter night","Story"),
    new Book(10,"Prema","Story")
  ];
  constructor() { 

  }

  getBooks(){
    return this.books;
  }
}
