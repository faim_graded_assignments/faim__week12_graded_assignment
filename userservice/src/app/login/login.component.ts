import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { LoginUserService } from '../service/login-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isPresent:boolean;

  constructor(private login:LoginUserService) { 

  }

  ngOnInit(): void {
  }

  loginUser(data:any){
    this.isPresent = this.login.getUser(data);
    if(this.isPresent){
      sessionStorage.setItem("email",data.email);
      // alert('You are Logged in Successfully');
      
      
    }else{
      alert('You are not registered');
    }
  }

  dashboard(frm){
    sessionStorage.setItem("email",frm.email);
    let app = new AppComponent();
    app.dashboard();
  }
}
