import { Component, OnInit } from '@angular/core';
import { Users } from '../models/users'
import { LoginUserService } from '../service/login-user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email:string;
  userList: Users[];
  constructor(private loginUser: LoginUserService) { }

  ngOnInit(): void {
  }

  registerUser(data: any) {
    this.email = data.email;
    if (this.email.endsWith("@hcl.com")) {
      this.loginUser.registerUser(data);
      alert("You Are Registered SuccessFully")
      console.log(data.email + " " + data.name + " " + data.password);
    }else{
      alert("Your Email is not of type admin");
    }
  }
}
