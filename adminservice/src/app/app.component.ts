import { Component } from '@angular/core';
import { BookService } from './bookservice/book.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private bookService:BookService){

  }

  btnAddBookPage=false;
  btnAddBook:boolean = false;
  btnDeleteBook:boolean = false;
  listOfBooks:any;
  name:string;
  email: string;
  btnLogOut: boolean = false;
  btnLogin: boolean = true;
  btnRegister: boolean = true;
  getLoginPage: boolean = false;
  getRegisterPage: boolean = false;
  btnShowBooks:boolean = true;
  getShowBooksPage: boolean = false;
  getDB: boolean = false;

  login() {
    this.btnShowBooks=true;
    this.btnLogin = false;
    this.btnRegister = true;
    this.getLoginPage = true;
    this.getRegisterPage = false;
  }

  register() {
    this.btnShowBooks=true;
    this.btnRegister = false;
    this.btnLogin = true;
    this.getLoginPage = false;
    this.getRegisterPage = true;
  }

  dashboard() {
    this.email = sessionStorage.getItem("email");
    if (this.email != null) {
      this.btnLogin = false;
      this.getLoginPage = false;
      this.getRegisterPage = false;
      this.btnRegister = false;
      this.btnLogOut = true;
      this.getDB = true;
      this.btnAddBook=true;
      this.btnDeleteBook= true;
      this.btnShowBooks = false;
      console.log("dashboard is working with "+ this.getDB);
      return this.getDB;
    }else{
      this.btnLogOut = false;
      return false;
    }
  }

  getBooks(){
    this.btnLogin=true;
    this.btnRegister=true;
    this.getLoginPage=false;
    this.getRegisterPage=false;
    this.btnShowBooks=false;
    this.email = sessionStorage.getItem("email");
    if(this.email == null && this.getShowBooksPage === false){
      this.getShowBooksPage = true;
      return false;
    }else{
      return true;
    }
  }


  logOut(){
    this.btnLogin=true;
    this.btnRegister=true;
    this.btnShowBooks=true;
    this.getLoginPage=true;
    this.getRegisterPage=false;
    this.getShowBooksPage=false;
    sessionStorage.removeItem("email");
    sessionStorage.clear()
  }

}
