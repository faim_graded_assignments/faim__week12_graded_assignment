import { Component, OnInit } from '@angular/core';
import { BookService } from '../bookservice/book.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  allBooks:any;
  constructor(private book:BookService) {
    this.allBooks = this.book.getBooks();
  }

  ngOnInit(): void {
  }

  


}
