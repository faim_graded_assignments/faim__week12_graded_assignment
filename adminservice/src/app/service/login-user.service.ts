import { Injectable } from '@angular/core';
import { Users } from '../models/users';

@Injectable({
  providedIn: 'root'
})
export class LoginUserService {
 
  userList:Users[]=[];
  constructor() {

  }

  registerUser(data:any){
    console.log(data.email+" "+data.name+" "+data.password);
    this.userList.push(new Users(data.email,data.name,data.password));
    // alert("You are Registered");
    console.log(this.userList);
  }

  getUser(data:any) {
    for(let i=0;i<this.userList.length;i++){
      if(this.userList[i].email === data.email && this.userList[i].password === data.password){
        return true;
      }
    }
    return false; 
  }

  getName(email:string){
    for(let i=0;i<this.userList.length;i++){
      if(this.userList[i].email === email){
        return this.userList[i].name;
      }
    }
  }
}
