import { Component, OnInit } from '@angular/core';
import {Users} from '../models/users'
import { LoginUserService } from '../service/login-user.service';
 
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userList:Users[];
  constructor(private loginUser:LoginUserService) { }

  ngOnInit(): void {
  }

  registerUser(data:any){
    // this.userList.push(new Users(data.email,data.name,data.password));
    this.loginUser.registerUser(data);
    alert("You Are Registered SuccessFully")
    console.log(data.email+" "+data.name+" "+data.password);
  }
}
