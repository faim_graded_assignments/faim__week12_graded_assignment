export class Book{
    public bookId:number;
    public bookName:string;
    public bookGenre:string;

    constructor(bookId,bookName,bookGenre){
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookGenre = bookGenre;
    }
}